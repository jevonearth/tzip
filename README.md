# Tezos Improvement Proposals

TZIP (pronounce "tee-zip") stands for Tezos Improvement Proposal, which are
documents that explain how the Tezos blockchain works or how it ought to work.

See [TZIP-1](TZIP-1.md)
